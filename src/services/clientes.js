import axiosInstance from './axios.js'

export const getClientes = async () => {
    try {
        const res = await axiosInstance.get('1/clientes')
        return res.data.cliente
    } catch (error) {
        console.error(error)
    }
}

export const guardarCliente = async (cliente) => {
    try {
        const res = await axiosInstance.post('1/clientes', cliente)
        console.log(res)
        return res
    } catch (error) {
        console.error(error)
    }
}