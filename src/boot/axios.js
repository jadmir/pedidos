import Vue from 'vue'
import axiosInstance from '../utils/axios'
import axiosLoad from '../utils/axios-load'

Vue.prototype.$api = axiosInstance
Vue.prototype.$api$ = axiosLoad
