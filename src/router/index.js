import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Dashboard from '../views/Dashboard.vue'
import Overview from '../views/Overview.vue'
import Profile from '../views/Profile.vue'
import Settings from '../views/Settings.vue'
import Messages from '../views/Messages.vue'
import New from '../views/New.vue'
import Cliente from '../views/Cliente.vue'
import Editar from '../views/Editar.vue'
import Nuevo from '../views/Nuevo.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/dashboard',
    component: Dashboard,
/*     children: [
      {
        path: '/dashboard',
        component: () => import('../views/Overview')
      },
      {
        path: '/messages',
        component: () => import('../views/Messages')
      },
      {
        path: '/profile',
        component: () => import('../views/Profile')
      },
      {
        path: '/settings',
        component: () => import('../views/Settings')
      },
    ] */
  },
  {
    path: '/overview',
    name: 'Overview',
    component: Overview,
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
  },
  {
    path: '/messages',
    name: 'Messages',
    component: Messages,
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
  },
  {
    path: '/new',
    name: 'New',
    component: New,
  },
  {
    path: '/cliente',
    name: 'Cliente',
    component: Cliente,
  }, 
  {
    path: '/editar/:dni',
    name: 'Editar',
    component: Editar,
  },
  {
    path: '/nuevo',
    name: 'Nuevo',
    component: Nuevo,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
